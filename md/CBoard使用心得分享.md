## 一、介绍

### 1.CBoard简介

CBoard 不仅是一款自助BI数据分析产品, 还是开放的BI产品开发平台

- 用户只需简单拖拽就能自助完成数据多维分析与报表设计
- 开发者能够简单扩展连接所有你的Java程序能够触及的数据

量励系统决策分析部分主要通过使用CBoard制作图表，组装看板，然后生成分享链接，再通过iframe集成分享链接的方式进行使用。



### 2.基本概念

#### 1、数据源

目前CBoard支持以下数据源连接

- JDBC数据源（涵盖几乎所有的关系型数据库）
- ElasticSearch 1.x, 2.x, 5.x (原生读取Index与Mapping，根据用户拖拽生成查询DSL)
- Kylin 1.6 (原生读取kylin Model，根据用户拖拽生成查询SQL)
- TextFile (文本文件，文本需要存放于CBoard应用服务器上面，读取本地文件)
- Saiku2.x (读取Saiku图表数据而非集成Saiku生成图表)
- Solr4.x, 5.x (读取Solr collection，根据用户拖拽生成Solr查询语法，4.x无法使用后台聚合功能)



#### 2、数据集/模型

CBoard没有详细区分数据集和模型的概念，两者都表示同一个东西。数据集类似于OLAP分析的Cube(数据立方体)，可以提前定义查询、聚合表达式、动态时间漏斗。在用户数据模型比较稳定的前提下，可以减少相同数据集下不同表报设计时重复的填写查询脚本、新建聚合表达式工作。



#### 3、图表

主要包含交叉表、明细表和22种Echarts图。



#### 4、看板

数据看板可以很方便的把一系列的多个图表组合在一个页面展示，之前社区版里看板布局叫网格布局和时间布局，商业版里看板布局叫做自由布局，虽然名字改了，但是其实都差不多，在使用过程中，感觉也不是那么“自由”，后续在踩坑里会提及到。



#### 5、驾驶舱/监控大屏

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E9%A9%BE%E9%A9%B6%E8%88%B1.png)

主要用于数据大屏展示，由于项目没使用到，暂时未研究具体的使用细节，大家可以去官网相关章节查看。

https://cboard_beta.gitee.io/ibi-doc/#/zh-cn/manual/cockpit



### 3.基本使用

#### 1、安装与配置

https://cboard_beta.gitee.io/ibi-doc/#/zh-cn/manual/install

大家可以参考官网安装与配置章节，这里不再赘述



#### 2、基本使用步骤

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8%E6%AD%A5%E9%AA%A4.png)



#### 3、系统主页面说明

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%B3%BB%E7%BB%9F%E4%B8%BB%E9%A1%B5%E9%9D%A2.png)



**自助分析：** 包括“新建图表”和“打开图表”两个选项。可以方便的新建图表或者打开已经新建的图表进行编辑。

**看板:**   主要包括“看板管理”、“网格布局”、“驾驶舱布局”三个选项。三个选项都是用于对看板的创建。

**配置：**包括“数据源管理”、“数据集（模型）管理”、“定时任务”。分别是对数据源和模型的管理，以及定时发送统计邮件的功能。

**管理：**主要是管理用户和用户的权限



#### 4、资源管理器/文件夹系统

CBoard的资源管理器是以树型的文件夹进行文件的管理的。下图演示的是打开图表的文件管理。

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E8%B5%84%E6%BA%90%E7%AE%A1%E7%90%86%E5%99%A8.png)

以量励项目为例，这里我们是将模型、图表、看板分别建立子文件夹，再在子文件夹下分别按项目模块建立二级目录，这样便于资源管理。

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/cboard%E7%9B%AE%E5%BD%95%E7%BB%93%E6%9E%84.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E9%A1%B9%E7%9B%AE%E7%9B%AE%E5%BD%95%E7%BB%93%E6%9E%84.png)



## 二、后端部分

### 1. 整体的流程图

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E6%95%B4%E4%BD%93%E7%9A%84%E6%B5%81%E7%A8%8B%E5%9B%BE.png)



 CBoard的整体的流程类似于java的分层设计，图表&&看板是视图层，用于对数据的展示，自定义图表SQL的建立模型是模型层。视图层直接依赖于模型层的数据，模型层则直接从数据库拿数据。



### 2. 数据源设置

 CBoard目前支持几乎所有的关系型数据库 & MongoDB & ES & 文本文件 & Solar等数据源，我们项目中主要使用的MySql,以下就是对于CBoard使用MySql作为数据源的教程分享说明，其他数据源配置可参考官方文档： [CBoard数据源配置](https://cboard_beta.gitee.io/ibi-doc/#/zh-cn/manual/datasource?id=_1-jdbc%e6%95%b0%e6%8d%ae%e6%ba%90)

- 点击配置->数据源管理->点击+号新增数据源->配置jdbc数据源

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE1.png)

- 配置好数据源连接信息（连接配置Properties、是否使用连接池、数据源聚合可选）点击测试按钮，写一行SQL测试数据源，提示成功，点击保存按钮保存数据源就OK了

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE3.png)



### 3. 数据集的介绍&&数据模型的建立

**数据集的 schema 包含维度列、度量列、聚合表达式、预定义漏斗**

CBoard模型建立

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/CBoard%E6%A8%A1%E5%9E%8B.png)



 以下是最简单的模型

 ```mysql
 select year as '年份',drg_code as 'drg编码',staff_outlay as '人员经费',staff_outlay_proportion as '人员经费占比'
 from cost_drg
 ```

- 度量列：最简单的SQL模型的staff_outlay，staff_outlay_proportion都是度量列

- 维度列：上面模型中能够group by 的字段就是维度列，例如 year, drg_code
  在CBoard生成图表的时候，拖入了维度列的 **year, drg_code**，指标列**人员经费，人员经费占比**，操作如下图：

  ![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E5%9B%BE%E8%A1%A8%E6%8B%96%E5%85%A5%E7%A4%BA%E4%BE%8B%E5%9B%BE.png)

  

  生成的查询SQL如下：

```mysql
 select year as c_0,drg_code as c_1,sum(staff_outlay),sum(staff_outlay_proportion)
 from ( select * from cost_drg
      ) cb_view
 group by year,drg_code 
```

- 聚合表达式(sum,count,avg,max,min,distinct)

​    其中distinct 函数类似于count(distinct staff_outlay)

​    创建如下表达式：求不同的维度下的人员经费之和

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E8%A1%A8%E8%BE%BE%E5%BC%8F.png)

 在CBoard生成图表的时候，拖入了维度列**year**，指标列**人员经费分组求和**的查询SQL如下：

```mysql
SELECT `year` AS c_0,
       SUM(`staff_outlay`)
  FROM (
         select * from cost_drg
) cb_view
 
 GROUP BY `year`
```



- 预定义漏斗（where）

  在where 后面追加 添加选择了的漏斗条件

**小结&&建议：**

CBoard模型就是一个通用的SQL的子查询。有点类似于MySQL的视图。

CBoard模型能复用的尽量复用，能写一个通用的SQL，尽量写通用的SQL便于后期的维护。



### 4. 环境变量配置

CBoard内置ibeetl模板引擎，详情参见官网：[Ibeetl官网](http://ibeetl.com/guide/#/beetl/)，所以CBoard的模型中可以使用ibeetl的语法，全局变量就是使用动态解析注入到SQL中的。

项目中使用到的ibeetl基本语法：

- 判断是否有某个变量

```javascript
if (has(year)) {
      print("有变量year")
  }
```

* 数组的连接

```
arr.joinString(variable, ”defaultValues")
arr.joinNumber(variable, “defaultValues")
```

CBoard模型中使用以上语法，全局变量的使用：

```mysql
<%
  var  where = "where 1 = 1";
  if (has(year)) {
      where = where + (" AND year = '" + year + "'");
  }
  if(has(drgCodeArray)){
      where = where + (" AND drg_code in ("+ arr.joinString(drgCodeArray) + ")");
  }
%>
select * from cost_drg ${where}  -- ${}  取值语法
```

* 环境变量使用案例之---联动（在上面的模型的基础之上）

1. 新建图表

   ![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E5%88%9B%E5%BB%BA%E5%9B%BE%E8%A1%A81.png)

2. 选择图表类型，拖入维度列和指标列,预览保存图表

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E5%88%9B%E5%BB%BA%E5%9B%BE%E8%A1%A82.png)



3. 图表嵌入看板，级联条件搜索

   ![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E5%B5%8C%E5%85%A5%E5%9B%BE%E8%A1%A8.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%AD%9B%E9%80%89%E6%9D%A1%E4%BB%B6%E6%B7%BB%E5%8A%A01.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%AD%9B%E9%80%89%E6%9D%A1%E4%BB%B6%E6%B7%BB%E5%8A%A02.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%AD%9B%E9%80%89%E6%9D%A1%E4%BB%B6%E6%B7%BB%E5%8A%A03.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%AD%9B%E9%80%89%E6%9D%A1%E4%BB%B6%E6%B7%BB%E5%8A%A04.png)

### 5. 踩坑记

​      **场景：**为了设计图标的时候不用起别名，在模型那一栏就使用 ‘as 中文汉字别名’，但是如果别名中带有特殊符号或者MySql关键字，在使用**明细表**的时候就会有大坑。

​                 例如：

```mysql
         select  xxx   as '2019年-2020年' from table
```

​                 在使用明细表的时候，会自动生成如下SQL：

```mysql
         select 2019年-2020年 as c_0 form 
         (
            select  xxx  as '2019年-2020年' from table
         )cb_view
         
```

这里的 **-** 就会导致Mysql 报错。

​      **解决方案：** 在组建模型的时候，如果遇到特殊字符或者关键字的时候，尽量使用英文，图表设计的时候自定义别名。目前遇到的特殊字符和关键字包括 **-  / or **



## 三、前端部分

### 1.图表设计

CBoard中图表设计主要涉及到表格和22种Echarts图，图表设计界面各部分功能示意图如下所示

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E5%9B%BE%E8%A1%A8%E8%AE%BE%E8%AE%A1%E7%A4%BA%E6%84%8F%E5%9B%BE.png)

<font color="#FF0000">**Tips：**</font>

1. 编辑查询：可以更换依赖模型或者编辑即席查询sql

   ![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%BC%96%E8%BE%91%E6%9F%A5%E8%AF%A2.png)

2. 跳转数据集设计：直接跳转到当前图表依赖的模型编辑页上

3. 其他配置基本都是见名知意


### 1、表格制作与样式定制

#####  例子1：交叉表

a.交叉表说明及效果

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E4%BA%A4%E5%8F%89%E8%A1%A81.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E4%BA%A4%E5%8F%89%E8%A1%A82.png)



b.设置样式

1、取别名：在指标列编辑别名

2、分段变色：通过在指标列设置值样式实现，比如这里需求是小于零的数据显示红色，交叉表需要在每个指标里分别编辑设置值样式

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E4%BA%A4%E5%8F%89%E8%A1%A8%E5%8F%98%E8%89%B2%E6%95%88%E6%9E%9C.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E4%BA%A4%E5%8F%89%E8%A1%A8%E5%8F%98%E8%89%B2%E8%AE%BE%E7%BD%AE.png)

#####  例子2：明细表

a.明细表说明及效果

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E6%98%8E%E7%BB%86%E8%A1%A81.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E6%98%8E%E7%BB%86%E8%A1%A82.png)



b.设置样式

1、取别名：在指标列编辑别名

2、分段变色：同样也是小于零显示红色，注意这里设置方法不同于交叉表，可以一次性批量编辑

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E6%98%8E%E7%BB%86%E8%A1%A8%E5%8F%98%E8%89%B2%E6%95%88%E6%9E%9C1.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E6%98%8E%E7%BB%86%E8%A1%A8%E5%8F%98%E8%89%B2%E8%AE%BE%E7%BD%AE1.png)

```javascript
formatter = function(colDef, value, row) {
    if(value <0 || value< '0%'){
        return `<span style="color:#FF4500;">${value}</span>`
    }
    return value; 
}
```



### 2、图表制作与样式定制

##### 例子1：四象限散点图

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/4%E8%B1%A1%E9%99%90%E6%95%A3%E7%82%B9%E5%9B%BE%E6%95%88%E6%9E%9C2.png)

a.两个行维分别对应Echarts散点图中的xAxis和yAxis

b.具体散点图设置，可以通过右侧配置里选项来设置，但是如果要做到高度定制且功能最完善，还是需要通过CBoard暴露出来的Echarts的option配置文件来设置，在本例中，我们采用在开发者模式下使用js代码配置的方式来按照需求定制散点图

```javascript
// 调整之前的Option   --xAxis和yAxis配置为数值
// console.log('调整前：', option)
// x轴辅助线
var markLineX = JSON.parse(option.series[0].data[0][2])[2]
// y轴辅助线
var markLineY = JSON.parse(option.series[0].data[0][2])[3]

var newData = _.chain(option.series[0].data)
  .each((item) => {
    //象限判定
    //象限判定
    let x = item[0]
    let y = item[1]

    let mX = markLineX
    let mY = markLineY
    let tempArr = JSON.parse(item[2])
    // 3,4象限
    if (mY - y >= 0) {
      // 3象限
      if (mX - x >= 0) {
        tempArr.push('3')
      } else {
        // 4象限
        tempArr.push('4')
      }
    } else {
      // 1,2象限
      // 2象限
      if (mX - x >= 0) {
        tempArr.push('2')
      } else {
        // 1象限
        tempArr.push('1')
      }
    }
    let tempStr = JSON.stringify(tempArr)
    item.splice(-1, 1, tempStr)
  })
  .value()

option.series[0].data = newData
// console.log('series：', option.series[0].data)
$$option = {
  legend: {},
  grid: {
    top: '10%',
    left: '5%',
    right: '5%',
    bottom: '5%',
  },
  tooltip: {
    formatter: function (params) {
      // console.log('调整前params：', params);
      return `
        <div>科室：${JSON.parse(params.data[2])[4]}</div>
        <div>病例数（例）：${JSON.parse(params.data[2])[5]}</div>
        <div>住院天数（天）：${JSON.parse(params.data[2])[6]}</div>
        <div>例均住院日（天）：${parseFloat(JSON.parse(params.data[2])[7]).toFixed(2)}</div>
        <div>床日结余（元）：${parseFloat(JSON.parse(params.data[2])[8]).toFixed(2)}</div>
        `
    },
  },
  xAxis: {
    name: '例均住院天数（天）',
    min: 0,
    // scale:true,
    nameLocation: 'middle',
    nameGap: 20,
    nameTextStyle: {
      color: '#000',
    },
    boundaryGap: false,
  },
  yAxis: {
    name: '床日结余（元）',
    nameLocation: 'middle',
    nameGap: -18,
    nameTextStyle: {
      color: '#000',
    },
  },
  series: [
    {
      markLine: {
        lineStyle: {
          type: 'solid',
        },
        data: [
          {
            name: '平均值(天)',
            xAxis: markLineX,
            label: {
              show: true,
              position: 'middle',
              color: '#222',
              formatter: function (params) {
                return '平均值（天）'
              },
            },
            tooltip: {
              show: true,
              formatter: function (params) {
                return params.data.value
              },
            },
          },
          {
            name: '平均值(元)',
            yAxis: markLineY,
            label: {
              show: true,
              position: 'middle',
              color: '#222',
              formatter: function (params) {
                return '平均值（元）'
              },
            },
            tooltip: {
              show: true,
              formatter: function (params) {
                return params.data.value
              },
            },
          },
        ],
      },
      label: {
        show: true,
        position: 'bottom',
        formatter: function (params) {
          return `${JSON.parse(params.data[2])[4]}`
        },
      },
      symbolSize: 13,
      itemStyle: {
        color: function (params) {
          let data = JSON.parse(params.data[2])
          // 象限
          let quadrant = data[data.length - 1]
          switch (quadrant) {
            case '1':
              return '#5479A9'
            case '2':
              return '#EA8D18'
            case '3':
              return '#D95657'
            case '4':
              return '#7EB7B2'
          }
        },
      },
    },
  ],
}
// 调整之后的Option
// $$option.afterMerged = function () {
//   console.log('调整后：', option)
// }
```

<font color="#FF0000">**Tips：**</font>

具体数据，通过打印设置前后option对象来调试



### 2.CBoard看板设计

#### 1、制作看板

一句话概括，之前做的无论是表格还是图表，都可以看做是一个一个组件，看板就是一个页面，在这里可以通过拖拽、引入等方式来填充页面内容，最后将看板分享生成分享链接，最后在项目中通过iframe嵌入的方式来集成。

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%9C%8B%E6%9D%BF%E7%BC%96%E8%BE%911.png)

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%9C%8B%E6%9D%BF%E7%BC%96%E8%BE%912.png)

红框部分就是看板进入可编辑状态的主要位置。



#### 2、集成看板

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E9%9B%86%E6%88%90%E7%9C%8B%E6%9D%BF1.png)



![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E9%9B%86%E6%88%90%E7%9C%8B%E6%9D%BF2.png)



### 3.踩坑

#### 1、被引用的表格、图表编辑保存后，在看板不生效

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E7%9C%8B%E6%9D%BF%E4%B8%8D%E7%94%9F%E6%95%88.png)



<font color="#FF0000">**解决方法：**</font>

编辑并保存图表后，在引用图表看板需要点击右上角三个点然后点击同步样式，在编辑图表时所做的修改才会生效，有时候需要多点击此同步样式。



#### 2、隐藏看板通用筛选框查询、重置按钮

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E9%9A%90%E8%97%8F%E6%8C%89%E9%92%AE1.png)



<font color="#FF0000">**解决方法：**</font>

在CBoard里并不能直接设置隐藏这2个按钮，可以通过修改全局css的方式来结局



![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E9%9A%90%E8%97%8F%E6%8C%89%E9%92%AE2.png)



#### 3、自定制表格列宽

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%A1%A8%E6%A0%BC%E5%AE%BD%E5%BA%A6.png)

<font color="#FF0000">**解决方法：**</font>

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%A1%A8%E6%A0%BC%E5%AE%BD%E5%BA%A62.png)



#### 4、饼图制作

需要先选择漏斗图，然后在开发模式中去修改图的类型

```javascript
// 调整之前的Option
// console.log(option);
$$option = {
   legend: {},
   grid: {},
   series:[{
       type:'pie'
   }]
   //.... 其他配置
}
// 调整之后的Option
//$$option.afterMerged = function() {
// console.log(option);
//}
```



#### 5、外部菜单高亮同步

iframe嵌入的CBoard的url跳转不会引起外层系统url的变化，那么怎么做到让外层系统左侧的菜单跟随高亮显示？

<font color="#FF0000">**解决方法：**</font>

1. 当集成iframe展示看板需要的js、css资源加载完毕之后，iframe会向父页面发送加载完毕消息,加载完毕消息类型为'LOG'。

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E8%8F%9C%E5%8D%95%E5%90%8C%E6%AD%A5%E9%AB%98%E4%BA%AE1.png)

​	利用这个消息可以拿到boardId，因为决策分析这一部分左侧菜单我们是通过后端接口动态生成的，只要递归去匹配后端动态返回决策分析菜单中的boardId，	即可知道当前展示的看板是哪一个。



2. 前端存储的从后端接口获取并组装数据生成的决策分析路由表

![](https://gitee.com/moumouniu/cboard-share-resources/raw/master/images/%E8%8F%9C%E5%8D%95%E5%90%8C%E6%AD%A5%E9%AB%98%E4%BA%AE2.png)

3. 实现关键代码片段

   src\views\decisionAnalysis\AnalysisCommon.vue

​	

```javascript
<template>
  <div class="base-data-list-wrapper whole-hospital-income">
    <div class="iframe-box">
      <div class="analysis-main-box">
        <iframe
          frameborder="0"
          class="iframe-contain"
          name="iframeContain"
          seamless
          scrolling="yes"
          :src="menuUrl"
        >
          您当前的浏览器不支持页面上的功能，请升级您当前的浏览器版本或使用谷歌浏览器访问当前页面
        </iframe>
      </div>
    </div>
  </div>
</template>

<script>
import api from '@/api/allocationSetting/allocationSolution.js';
import baseDataListMixin from '@/mixins/baseDataListMixin';
import { mapActions } from 'vuex';

export default {
  name: 'AnalysisCommon',
  mixins: [baseDataListMixin],
  data() {
    return {
      api,
      menuUrl: '',
      timer: undefined
    };
  },
  methods: {
    ...mapActions(['user/_getInfo']),
    // 递归匹配方法
    judgePushTo(currentBoardId, decissionRoutes) {
      if (decissionRoutes.length === 0) {
        return;
      }
      for (let i = 0; i < decissionRoutes.length; i++) {
        let item = decissionRoutes[i];
        let { boardId } = item.meta;
        if (boardId == currentBoardId) {
          return item.path;
        }
        if (item.children && item.children.length > 0) {
          let path = this.judgePushTo(currentBoardId, item.children);
          if (path) {
            return item.path + '/' + path;
          }
        }
      }
      return '';
    }
  },

  created() {
    this.menuUrl = this.$route.meta.url;
    // 每隔15分钟去刷新一次token
    this.timer = setInterval(() => {
      this['user/_getInfo']();
    }, 1000 * 60 * 15);
  },
  mounted() {
    let router = this.$router;
    let route = this.$route;
    // 异步路由表
    let asyncRoutesList = JSON.parse(window.localStorage.getItem('addRoutes'));
    // 决策路由表
    let decissionRoutes = asyncRoutesList.filter((v) => v.name === 'DecisionAnalysis');

    // 等待iframe资源准备完毕立即发送加载看板消息
    window.addEventListener('message', (event) => {
      let { type, boardId } = event.data;
      if (type === 'LOG') {
        // 匹配boardId，然后跳转
        let res = this.judgePushTo(boardId, decissionRoutes);
        if (route.path === res) return;
        this.$eventbus.$emit('Analysis',res)//触发菜单高亮

      }
    });
  },
  beforeDestroy() {
    clearInterval(this.timer);
  }
};
</script>
```



src\layout\components\sideBar\index.vue

```javascript
<template>
  <div class="sideBar" id="domSideBar">
    <el-scrollbar>
      <el-menu
        :default-active="activeMenu"
        class="el-menu-vertical-demo"
        background-color="#432758"
        text-color="#b5b6bd"
        active-text-color="#fff"
        mode="vertical"
        :collapse-transition="false"
        :collapse="opened"
      >
        <sidebar-item
          v-for="item in routes"
          :key="item.path"
          :item="item"
          :basePath="item.path"
        ></sidebar-item>
      </el-menu>
    </el-scrollbar>
  </div>
</template>

<script>
import SidebarItem from './SideBarItem'
import { mapGetters } from 'vuex'
export default {
  components: { SidebarItem },
  data() {
    return {
      pathMenu: null,
    };
  },
  computed: {
    ...mapGetters(['routes', 'opened']),
    // booleanOpen() {
    //   return this.opened === 'true' ? true : false
    // },
    activeMenu() {
      let path = this.$route.path
      if(this.pathMenu !=null){
        path = this.pathMenu
      }
      return path;
    },

  },
  methods: {

  },
  mounted() {
    //这里使用监听器针对决策分析的iframe中cboard下钻页面的同步高亮
    //----AnalysisCommon页面触发
    this.$eventbus.$on('Analysis',(menu)=>{
      this.pathMenu = menu
    })
  }
}
</script>
```



## 四、Q&A

### 1.用CBoard制作页面步骤？

### 2.看板环境变量配置时候的如果key和模型中has(key)语法的key的名称不一样，能取到数据吗？



## 五、相关资料

### 1.CBoard官方文档

https://cboard_beta.gitee.io/ibi-doc/#/zh-cn/



### 2.CBoard官方培训资料

https://gitee.com/moumouniu/cboard-share-resources/blob/master/docs/IBI%E5%8A%9F%E8%83%BD%E4%B8%8E%E6%93%8D%E4%BD%9C%E5%9F%B9%E8%AE%ADv1.6.pptx



### 3.Echarts配置项文档

https://echarts.apache.org/zh/option.html#title